export interface ICustomTable {
  headers: ITableHeaders[];
  data: any[];
}

export interface ITableHeaders {
  name: string;
}
