import {
  Control,
  FieldErrors,
  FieldValues,
  UseFormGetValues,
} from "react-hook-form";
import { BaseSyntheticEvent } from "react";

export interface IForm<T extends FieldValues> {
  errors: FieldErrors<T>;
  control: Control<T, object>;
  onSubmit: (e?: BaseSyntheticEvent<any> | undefined) => Promise<any>;
  isValid: boolean;
  loading: boolean;
  getValues?: UseFormGetValues<T>;
}
