import { RolesEnum, PathEnum } from "@infrastructure/enum";
import {
  MdHouse,
  MdShoppingBasket,
  MdStorage,
  MdSupervisedUserCircle,
} from "react-icons/md";
import { IconType } from "react-Icons";

export interface NavigationRole {
  name: string;
  Icon: IconType;
  roles: RolesEnum;
  to: string;
}

export const navigation_roles: NavigationRole[] = [
  {
    Icon: MdHouse,
    name: "Dashboard",
    roles:
      RolesEnum.ADMIN ||
      RolesEnum.SELLER ||
      RolesEnum.CLIENT ||
      RolesEnum.INVENTORY,
    to: PathEnum.DASHBOARD,
  },
  {
    Icon: MdSupervisedUserCircle,
    name: "Users",
    roles: RolesEnum.ADMIN,
    to: PathEnum.USERS,
  },
  {
    Icon: MdStorage,
    name: "Products",
    roles: RolesEnum.ADMIN || RolesEnum.SELLER,
    to: PathEnum.PRODUCTS,
  },
  {
    Icon: MdShoppingBasket,
    name: "Orders",
    roles: RolesEnum.SELLER,
    to: PathEnum.ORDERS,
  },
];
