import { environment } from "@environment/environment";

export const API_ROUTES = {
  AUTH: `${environment.ciUrl}/user/sign-in`,
  GET_USERS: `${environment.ciUrl}/users`,
};
