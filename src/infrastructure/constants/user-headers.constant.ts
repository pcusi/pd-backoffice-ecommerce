import { ITableHeaders } from "@infrastructure/interfaces/table.interfaces";
import { getHeaderLabelCatalog } from "@infrastructure/catalog";
import { UserHeadersEnum } from "@infrastructure/enum";

export const userHeaders: ITableHeaders[] = [
  {
    name: getHeaderLabelCatalog[UserHeadersEnum.EMAIL],
  },
  {
    name: getHeaderLabelCatalog[UserHeadersEnum.NAMES],
  },
  {
    name: getHeaderLabelCatalog[UserHeadersEnum.LASTNAMES],
  },
  { name: getHeaderLabelCatalog[UserHeadersEnum.ROLE] },
];

export const mapUserHeaders: string[] = [
  UserHeadersEnum.EMAIL,
  UserHeadersEnum.NAMES,
  UserHeadersEnum.LASTNAMES,
  UserHeadersEnum.ROLE,
];
