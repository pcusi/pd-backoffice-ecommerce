import { RolesEnum } from "@infrastructure/enum";

export const rolesCatalog: Record<string, string> = {
  [RolesEnum.ADMIN]: "administrador",
  [RolesEnum.CLIENT]: "cliente",
  [RolesEnum.INVENTORY]: "logística",
  [RolesEnum.SELLER]: "vendedor",
};
