import { PathEnum } from "@infrastructure/enum";

export const routesCatalog: Record<string, string> = {
  [PathEnum.DASHBOARD]: "Dashboard",
  [PathEnum.ORDERS]: "Ventas",
  [PathEnum.USERS]: "Usuarios",
  [PathEnum.PRODUCTS]: "Productos",
};
