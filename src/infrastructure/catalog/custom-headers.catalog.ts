import { UserHeadersEnum } from "@infrastructure/enum";

export const getHeaderLabelCatalog: Record<string, string> = {
  [UserHeadersEnum.EMAIL]: "Correo electrónico",
  [UserHeadersEnum.LASTNAMES]: "Apellidos",
  [UserHeadersEnum.NAMES]: "Nombres",
  [UserHeadersEnum.ROLE]: "Rol",
};
