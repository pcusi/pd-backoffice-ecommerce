export enum UserHeadersEnum {
  EMAIL = "email",
  LASTNAMES = "lastnames",
  NAMES = "names",
  ROLE = "role",
}
