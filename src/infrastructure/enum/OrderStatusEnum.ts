export enum OrderStatusEnum {
  IN_PROCESS = "in_process",
  WAITING = "waiting",
  SEND = "send",
  DELIVERED = "delivered",
  CANCELED = "canceled",
  RETURNED = "returned",
}
