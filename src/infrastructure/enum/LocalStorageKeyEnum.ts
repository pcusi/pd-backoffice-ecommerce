export enum LocalStorageKeyEnum {
  AUTH_TOKEN = "authToken",
  USER = "user",
}
