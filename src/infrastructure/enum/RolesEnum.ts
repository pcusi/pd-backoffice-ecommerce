export enum RolesEnum {
  CLIENT = "client",
  ADMIN = "admin",
  SELLER = "seller",
  INVENTORY = "inventory",
}
