export enum PathEnum {
  DASHBOARD = "dashboard",
  PRODUCTS = "products",
  ORDERS = "orders",
  USERS = "users",
}
