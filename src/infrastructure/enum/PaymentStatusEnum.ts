export enum PaymentStatusEnum {
  PAYED = "payed",
  WAITING = "waiting",
  CANCELED = "canceled",
  REFUND = "refunded",
  PARTIALLY = "refunded_partially",
}
