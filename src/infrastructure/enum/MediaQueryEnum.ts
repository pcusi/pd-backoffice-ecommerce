export enum MediaQueryEnum {
  MOBILE = "@media (min-width: 340px) and (max-width: 720px)",
  TABLET = "@media (min-width: 720px) and (max-width: 1280px)",
  DESKTOP = "@media (min-width: 1280px)",
}
