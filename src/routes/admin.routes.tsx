import { createBrowserRouter, Navigate } from "react-router-dom";
import { SignIn } from "@components/auth";
import React from "react";
import { Admin } from "@layouts/admin/Admin";
import { Dashboard, Users } from "@components/admin";
import { PathEnum } from "@infrastructure/enum";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Navigate to="/auth" replace />,
  },
  {
    path: "/auth",
    children: [
      {
        path: "/auth",
        element: <Navigate to="/auth/sign-in" replace />,
      },
      {
        path: "sign-in",
        element: <SignIn />,
      },
    ],
  },
  {
    path: "/admin",
    element: <Admin />,
    children: [
      {
        path: PathEnum.DASHBOARD,
        element: <Dashboard />,
      },
      {
        path: PathEnum.USERS,
        element: <Users />,
      },
      {
        path: PathEnum.PRODUCTS,
        element: <></>,
      },
      {
        path: PathEnum.ORDERS,
        element: <></>,
      },
    ],
  },
]);
