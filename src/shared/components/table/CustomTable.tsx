import React, { FC } from "react";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import { ICustomTable } from "@infrastructure/interfaces/table.interfaces";
import { mapUserHeaders } from "@infrastructure/constants";

export interface CustomTableProps extends ICustomTable {}

export const CustomTable: FC<CustomTableProps> = ({ headers, data }) => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            {headers.map(({ name }) => (
              <TableCell key={Math.random()}>{name}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, index) => (
            <TableRow
              key={Math.random()}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              {headers.map((_, index) => (
                <TableCell component="th" scope="row" key={index}>
                  {row[mapUserHeaders[index]]}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
