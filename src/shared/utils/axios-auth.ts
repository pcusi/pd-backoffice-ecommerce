import axios from "axios";
import { LocalStorageKeyEnum } from "@infrastructure/enum";

const instance = axios.create();

instance.interceptors.request.use(
  async (config) => {
    config.headers = {
      Authorization: `${localStorage.getItem(LocalStorageKeyEnum.AUTH_TOKEN)}`,
    };
    return config;
  },
  (error) => {
    Promise.reject(error).then((r) => r);
  }
);

export default instance;
