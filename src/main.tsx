import React from "react";
import { router } from "@routes/admin.routes";
import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "@store/store";
import { GlobalStyles, ThemeProvider } from "@mui/material";
import { theme } from "@themes/theme";
import { globalStyles } from "./global.styles";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <GlobalStyles styles={globalStyles} />
      <ThemeProvider theme={theme}>
        <RouterProvider router={router} />
      </ThemeProvider>
    </Provider>
  </React.StrictMode>
);
