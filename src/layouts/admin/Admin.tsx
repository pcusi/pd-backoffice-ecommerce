import React from "react";
import { Outlet } from "react-router";
import { Box, Stack } from "@mui/material";
import {
  AdminGuard,
  adminStyles,
  Navbar,
  Sidebar,
  useAdminState,
} from "@components/admin";

export const Admin = () => {
  const { user, anchorEl, open, popoverClose, popoverClick } = useAdminState();

  return (
    <Box sx={adminStyles.admin}>
      <Sidebar />
      <Box sx={adminStyles.container}>
        <Navbar
          user={user}
          anchorEl={anchorEl}
          popoverClick={popoverClick}
          popoverClose={popoverClose}
          open={open}
        />
        <Stack sx={adminStyles.wrapper}>
          <AdminGuard>
            <Outlet />
          </AdminGuard>
        </Stack>
      </Box>
    </Box>
  );
};
