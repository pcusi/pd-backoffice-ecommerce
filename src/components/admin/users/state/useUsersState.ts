import { UsersFetch } from "@store/reducers/user/user.slice";
import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "@store/hooks/store.hook";
import { getUsers } from "@store/thunks/users/users.thunks";
import { ITableHeaders } from "@infrastructure/interfaces/table.interfaces";
import { userHeaders } from "@infrastructure/constants";

export interface UseUsersStateProps {
  fetch: UsersFetch;
  headers: ITableHeaders[];
}

export const useUsersState = (): UseUsersStateProps => {
  const dispatch = useAppDispatch();
  const { fetch } = useAppSelector((state) => ({ ...state.user }));

  const fetchUsers = () => {
    dispatch(getUsers());
  };

  useEffect(() => {
    if (fetch.users.length === 0) {
      fetchUsers();
    }
  }, [fetch.users]);

  return { fetch, headers: userHeaders };
};
