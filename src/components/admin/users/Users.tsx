import React from "react";

import { Button, Grid, Stack, Typography } from "@mui/material";
import { routesCatalog } from "@infrastructure/catalog";
import { PathEnum } from "@infrastructure/enum";
import { useUsersState } from "@components/admin";
import { CustomTable } from "@shared/components";
import { userLabels } from "@infrastructure/constants";

export const Users = () => {
  const { fetch, headers } = useUsersState();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Stack
          direction="row"
          justifyContent="space-between"
          alignItems="center"
        >
          <Typography variant="h1" color="text.dark" mb={2}>
            {routesCatalog[PathEnum.USERS]}
          </Typography>
          <Button variant="contained" size="large">
            {userLabels.button}
          </Button>
        </Stack>
      </Grid>
      <Grid item xs={12}>
        <CustomTable headers={headers} data={fetch.users} />
      </Grid>
    </Grid>
  );
};
