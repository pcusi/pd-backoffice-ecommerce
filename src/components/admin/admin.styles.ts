import { StylesProps } from "@shared/styles.props";
import { AdminLayoutEnum } from "@infrastructure/enum";

export const adminStyles: StylesProps = {
  container: {
    marginLeft: AdminLayoutEnum.SIDEBAR,
  },
  wrapper: {
    mt: 7,
    p: 4,
    height: "100vh",
  },
};
