import { useState, MouseEvent } from "react";
import { UserResponse } from "types/user_schema";
import { LocalStorageKeyEnum } from "@infrastructure/enum";
import { NavbarProps } from "@components/admin";

export interface UseAdminStateProps extends NavbarProps {}

export const useAdminState = (): UseAdminStateProps => {
  const user: UserResponse = JSON.parse(
    `${localStorage.getItem(LocalStorageKeyEnum.USER)}`
  );

  const [anchorEl, setAnchorEl] = useState<HTMLDivElement | null>(null);

  const handleClick = (event: MouseEvent<HTMLDivElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return {
    user,
    popoverClick: handleClick,
    popoverClose: handleClose,
    open,
    anchorEl,
  };
};
