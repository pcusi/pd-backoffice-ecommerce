import { useEffect, useState } from "react";
import { NavigationRole, navigation_roles } from "@infrastructure/constants";
import { UserResponse } from "types/user_schema";
import { LocalStorageKeyEnum } from "@infrastructure/enum";

export interface UseSidebarStateProps {
  menu: NavigationRole[];
}

export const useSidebarState = (): UseSidebarStateProps => {
  const user: UserResponse = JSON.parse(
    `${localStorage.getItem(LocalStorageKeyEnum.USER)}`
  );
  const [menu, setMenu] = useState<NavigationRole[]>([]);

  useEffect(() => {
    if (menu.length === 0) {
      setMenu(navigation_roles.filter((item) => item.roles === user.role));
    }
  }, [menu]);

  return {
    menu,
  };
};
