import React from "react";
import { Card } from "@mui/material";
import { Navigation, sidebarStyles, useSidebarState } from "@components/admin";

export const Sidebar = () => {
  const { menu } = useSidebarState();

  return (
    <Card sx={sidebarStyles.sidebar}>
      <Navigation menu={menu} />
    </Card>
  );
};
