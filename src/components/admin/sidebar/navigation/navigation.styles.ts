import { StylesProps } from "@shared/styles.props";
import { Palette } from "@themes/theme";

export const navigationStyles: StylesProps = {
  item: {
    ".icon.active": {
      fontSize: Palette.fontSize.h2,
      padding: 0.25,
      color: Palette.text.white,
      borderRadius: 2,
    },
    ".icon": {
      fontSize: Palette.fontSize.h2,
      padding: 0.25,
      borderRadius: 2,
    },
    ".tab-active": {
      color: Palette.text.white,
      backgroundColor: "secondary.main",
      padding: 0.5,
      ".MuiTypography-body1": {
        textDecoration: "none",
      },
    },
    ".tab-non-active": {
      color: Palette.text.dark,
      padding: 0.5,
    },
    ".link-active": {
      textDecoration: "none",
    },
    ".link-non-active": {
      textDecoration: "none",
    },
  },
};
