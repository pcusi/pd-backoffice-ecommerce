import React, { FC } from "react";

import { navigationStyles } from "@components/admin";
import { NavigationRole } from "@infrastructure/constants";
import { routesCatalog } from "@infrastructure/catalog";
import { Grid, Stack, Typography } from "@mui/material";
import { NavLink } from "react-router-dom";

export interface NavigationProps {
  menu: NavigationRole[];
}

export const Navigation: FC<NavigationProps> = ({ menu }) => {
  return (
    <Grid container spacing={2}>
      {menu.map(({ Icon, name, to }) => (
        <Grid item xs={12} key={Math.random()} sx={navigationStyles.item}>
          <NavLink
            to={to}
            className={({ isActive }) =>
              isActive ? "link-active" : "link-non-active"
            }
            children={({ isActive }) => (
              <Stack
                direction="row"
                alignItems="center"
                spacing={2}
                className={isActive ? "tab-active" : "tab-non-active"}
              >
                <Icon className={isActive ? "icon active" : "icon"} />
                <Typography variant="body1">{routesCatalog[to]}</Typography>
              </Stack>
            )}
          />
        </Grid>
      ))}
    </Grid>
  );
};
