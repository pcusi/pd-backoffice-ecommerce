import { StylesProps } from "@shared/styles.props";
import { AdminLayoutEnum, MediaQueryEnum } from "@infrastructure/enum";

export const sidebarStyles: StylesProps = {
  sidebar: {
    position: "fixed",
    height: "100vh",
    borderRadius: 0,
    padding: 4,
    top: 0,
    zIndex: 99,
    maxWidth: AdminLayoutEnum.SIDEBAR,
    backgroundColor: "primary.light",
    [MediaQueryEnum.MOBILE]: {
      height: "auto",
    },
  },
};
