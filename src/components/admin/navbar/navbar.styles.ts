import { StylesProps } from "@shared/styles.props";
import { Palette } from "@themes/theme";
import { AdminLayoutEnum, MediaQueryEnum } from "@infrastructure/enum";

export const navbarStyles: StylesProps = {
  navbar: {
    position: "fixed",
    top: 0,
    left: AdminLayoutEnum.SIDEBAR,
    width: `calc(100% - ${AdminLayoutEnum.SIDEBAR})`,
    backgroundColor: Palette.primary.glass,
    padding: 2,
    zIndex: 99,
    backdropFilter: "blur(5px)",
    [MediaQueryEnum.MOBILE]: {
      position: "relative",
      width: "100%",
      left: 0,
    },
  },
  avatar: {
    width: "30px",
    height: "30px",
    borderRadius: 1,
    cursor: "pointer",
  },
};
