import React, { FC } from "react";

import { Avatar, Box, Popover, Stack, Typography } from "@mui/material";
import { UserResponse } from "types/user_schema";
import { get } from "lodash";
import { navbarStyles } from "@components/admin";
import { rolesCatalog } from "@infrastructure/catalog";

export interface NavbarProps {
  user: UserResponse;
  anchorEl: HTMLDivElement | null;
  open: boolean;
  popoverClose: () => void;
  popoverClick: (event: React.MouseEvent<HTMLDivElement>) => void;
}

export const Navbar: FC<NavbarProps> = ({
  user,
  anchorEl,
  open,
  popoverClick,
  popoverClose,
}) => {
  return (
    <Box sx={navbarStyles.navbar}>
      <Stack direction="row" spacing={2} justifyContent="space-between">
        <Stack></Stack>
        <Stack direction="row" alignItems="center" spacing={2}>
          <Stack direction="column" spacing={0.5}>
            <Typography variant="body2" color="text.dark" textAlign="right">
              {get(user, "names")}
            </Typography>
            <Typography variant="caption" color="text.dark" textAlign="right">
              {rolesCatalog[get(user, "role", "")]}
            </Typography>
          </Stack>
          <Avatar
            src=""
            variant="rounded"
            onClick={popoverClick}
            sx={navbarStyles.avatar}
          />
          <Popover
            open={open}
            anchorEl={anchorEl}
            onClose={popoverClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
          >
            <Typography variant="h1" color="text.white">
              {"Holi"}
            </Typography>
          </Popover>
        </Stack>
      </Stack>
    </Box>
  );
};
