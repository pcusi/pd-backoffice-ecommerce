import { LocalStorageKeyEnum } from "@infrastructure/enum";

export interface UseAdminGuardStateProps {
  token: string;
}

export const useAdminGuardState = (): UseAdminGuardStateProps => {
  const token: string = `${localStorage.getItem(
    LocalStorageKeyEnum.AUTH_TOKEN
  )}`;

  return {
    token,
  };
};
