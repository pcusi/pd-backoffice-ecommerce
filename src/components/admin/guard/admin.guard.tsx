import React, { FC, ReactElement } from "react";

import { useAdminGuardState } from "@components/admin";
import { Navigate } from "react-router-dom";

export interface AdminGuardProps {
  children: ReactElement;
}

export const AdminGuard: FC<AdminGuardProps> = ({ children }) => {
  const { token } = useAdminGuardState();

  if (token === null) {
    return <Navigate to="/auth/sign-in" replace />;
  }

  return children;
};
