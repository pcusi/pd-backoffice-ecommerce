import React from "react";

import { Typography } from "@mui/material";

export const Dashboard = () => {
  return (
    <Typography variant="h1" color="text.dark">
      {"Dashboard"}
    </Typography>
  );
};
