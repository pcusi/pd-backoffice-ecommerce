import { StylesProps } from "@shared/styles.props";

export const authStyles: StylesProps = {
  container: {
    height: "100vh",
  },
  card: {
    height: "100%",
    px: 4,
    py: 10,
    borderRadius: 0,
    backgroundColor: "primary.dark",
  },
};
