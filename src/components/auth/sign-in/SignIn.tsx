import React from "react";

import { Card, Grid, Stack, Typography } from "@mui/material";
import { authStyles, SignInForm, useSignInState } from "@components/auth";

export const SignIn = () => {
  const { control, errors, isValid, onSubmit, loading } = useSignInState();

  return (
    <Grid container sx={authStyles.container}>
      <Grid item lg={8.5}></Grid>
      <Grid item lg={3.5} xs={12}>
        <Card sx={authStyles.card}>
          <Grid container spacing={2}>
            <Grid item xs={12} mb={3}>
              <Stack direction="column" spacing={2}>
                <Typography variant="h3" color="text.primary">
                  {"Welcome to Demo!"} 🤝
                </Typography>
                <Typography variant="body1" color="text.secondary">
                  {"Please sign-in to your account and start the adventure"}
                </Typography>
              </Stack>
            </Grid>
            <Grid item xs={12}>
              <SignInForm
                onSubmit={onSubmit}
                loading={loading}
                isValid={isValid}
                errors={errors}
                control={control}
              />
            </Grid>
          </Grid>
        </Card>
      </Grid>
    </Grid>
  );
};
