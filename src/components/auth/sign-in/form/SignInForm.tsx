import React, { FC } from "react";
import { Grid, InputLabel, TextField } from "@mui/material";
import { Controller } from "react-hook-form";
import { IForm } from "@infrastructure/interfaces/form.interfaces";
import { UserResponse } from "types/user_schema";
import { LoadingButton } from "@mui/lab";

export interface SignInFormProps extends IForm<UserResponse> {}

export const SignInForm: FC<SignInFormProps> = ({
  control,
  errors,
  isValid,
  loading,
  onSubmit,
}) => {
  return (
    <form onSubmit={onSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <InputLabel shrink htmlFor="bootstrap-input">
            Email
          </InputLabel>
          <Controller
            name="email"
            control={control}
            rules={{ required: true }}
            render={({ field }) => (
              <TextField
                fullWidth
                type="text"
                size="small"
                {...field}
                error={Boolean(errors.email)}
                helperText={
                  errors.email && "La dirección no puede estar vacía."
                }
                placeholder={"testing@gmail.com"}
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Grid item xs={12} mb={1}>
          <InputLabel shrink htmlFor="bootstrap-input">
            Password
          </InputLabel>
          <Controller
            name="password"
            control={control}
            rules={{ required: true }}
            render={({ field }) => (
              <TextField
                fullWidth
                size="small"
                type="password"
                {...field}
                error={Boolean(errors.password)}
                helperText={
                  errors.password && "La dirección no puede estar vacía."
                }
                placeholder={"Your password"}
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Grid item xs={12}>
          <LoadingButton
            loadingPosition="end"
            endIcon={<></>}
            disabled={!isValid}
            fullWidth
            variant="contained"
            type="submit"
            loading={loading}
          >
            {"Log In"}
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};
