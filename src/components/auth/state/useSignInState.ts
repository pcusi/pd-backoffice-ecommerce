import { useForm } from "react-hook-form";
import { UserResponse } from "types/user_schema";
import { IForm } from "@infrastructure/interfaces/form.interfaces";
import { useAppDispatch, useAppSelector } from "@store/hooks/store.hook";
import { signIn } from "@store/thunks/auth/auth.thunks";
import { useEffect, useState } from "react";
import { LocalStorageKeyEnum } from "@infrastructure/enum";

export interface UseSignInStateProps extends IForm<UserResponse> {}

export const useSignInState = (): UseSignInStateProps => {
  const [token] = useState<string | null>(
    localStorage.getItem(LocalStorageKeyEnum.AUTH_TOKEN)
  );
  const {
    control,
    formState: { isValid, errors },
    handleSubmit,
  } = useForm<UserResponse>({
    defaultValues: {
      email: "",
      password: "",
    },
    mode: "onChange",
  });
  const { loading } = useAppSelector((state) => ({ ...state.auth }));
  const dispatch = useAppDispatch();

  const logIn = handleSubmit(async (user) => {
    dispatch(signIn(user));
  });

  useEffect(() => {
    if (token !== null) {
      window.location.href = "/admin/dashboard";
    }
  }, [token]);

  return {
    control,
    errors,
    isValid,
    onSubmit: logIn,
    loading,
  };
};
