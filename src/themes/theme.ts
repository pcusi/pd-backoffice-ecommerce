import { createTheme } from "@mui/material";
import { ThemeOptions } from "@mui/system";

export const Palette = {
  primary: {
    main: "#F8F7FA",
    light: "#FFFFFF",
    glass: "rgba(255, 255, 255, .5)",
  },
  secondary: {
    dark: "#33303C",
    main: "#8B7CF0",
  },
  text: {
    primary: "#CBCCD2",
    grey: "#9496A3",
    border: "#272D48",
    white: "#FFF",
    dark: "#33303C",
  },
  montserrat: "Montserrat",
  ["montserrat-bold"]: "Montserrat-Bold",
  fontSize: {
    h1: "32px",
    h2: "28px",
    h3: "24px",
    h4: "20px",
    h5: "18px",
    h6: "16px",
    body1: "14px",
    body2: "12px",
    overline: "14px",
    caption: "10px",
  },
};

export const themeOptions: ThemeOptions = {
  palette: {
    primary: {
      main: Palette.primary.main,
      light: Palette.primary.light,
    },
    secondary: {
      main: Palette.secondary.main,
      dark: Palette.secondary.dark,
    },
    text: {
      primary: Palette.text.primary,
      secondary: Palette.text.grey,
      white: Palette.text.white,
      dark: Palette.text.dark,
    },
  },
};

export const theme = createTheme({
  palette: { mode: "light", ...themeOptions.palette },
  components: {
    MuiInputLabel: {
      styleOverrides: {
        root: {
          fontSize: Palette.fontSize.h6,
          color: Palette.text.grey,
        },
      },
    },
    MuiButton: {
      defaultProps: {
        disableRipple: true,
        disableElevation: true,
      },
      styleOverrides: {
        contained: {
          backgroundColor: Palette.secondary.main,
          border: "none",
          color: Palette.text.white,
          fontSize: Palette.fontSize.body1,
          fontWeight: 400,
          textTransform: "none",
          "&:disabled": {
            backgroundColor: Palette.secondary.dark,
            color: Palette.text.white,
          },
          "&:hover": {
            backgroundColor: Palette.secondary.main,
          },
        },
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          border: `1px solid ${Palette.text.grey}`,
          "&:hover": {
            border: `1px solid ${Palette.text.grey}`,
          },
          "&.Mui-focused": {
            border: `1px solid ${Palette.secondary.main}`,
          },
        },
        notchedOutline: {
          border: `none`,
        },
      },
    },
  },
  typography: {
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
    h1: {
      fontSize: Palette.fontSize.h1,
      fontFamily: Palette.montserrat,
    },
    h2: {
      fontSize: Palette.fontSize.h2,
      fontFamily: Palette.montserrat,
    },
    h3: {
      fontSize: Palette.fontSize.h3,
      fontFamily: Palette.montserrat,
    },
    h4: {
      fontSize: Palette.fontSize.h4,
      fontFamily: Palette.montserrat,
    },
    h5: {
      fontSize: Palette.fontSize.h5,
      fontFamily: Palette.montserrat,
    },
    h6: {
      fontSize: Palette.fontSize.h6,
      fontFamily: Palette.montserrat,
    },
    body1: {
      fontSize: Palette.fontSize.body1,
      fontFamily: Palette.montserrat,
    },
    body2: {
      fontSize: Palette.fontSize.body2,
      fontFamily: Palette.montserrat,
    },
    caption: {
      fontSize: Palette.fontSize.caption,
      fontFamily: Palette.montserrat,
    },
  },
});
