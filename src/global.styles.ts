import { Palette } from "@themes/theme";
import { GlobalStylesProps as StyledGlobalStylesProps } from "@mui/styled-engine/GlobalStyles/GlobalStyles";
import { Theme } from "@mui/system";

export const globalStyles: StyledGlobalStylesProps<Theme>["styles"] = {
  body: {
    backgroundColor: Palette.primary.main,
    padding: 0,
    margin: 0,
  },
};
