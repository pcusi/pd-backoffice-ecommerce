import { createAsyncThunk } from "@reduxjs/toolkit";
import { UserResponse } from "types/user_schema";
import axios from "@shared/utils/axios-auth";
import { API_ROUTES } from "@infrastructure/constants";

export const getUsers = createAsyncThunk<UserResponse[], undefined>(
  "/users",
  async () => {
    const response = await axios.get(API_ROUTES.GET_USERS);

    return response.data.users;
  }
);
