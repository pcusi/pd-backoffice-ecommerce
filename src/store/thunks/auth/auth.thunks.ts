import { createAsyncThunk } from "@reduxjs/toolkit";
import { SignInResponse } from "types/sign_in_response_schema";
import { UserResponse } from "types/user_schema";
import axios from "axios";
import { API_ROUTES } from "@infrastructure/constants/api.constant";

export const signIn = createAsyncThunk<SignInResponse, UserResponse>(
  "/auth/sign-in",
  async (payload) => {
    const response = await axios.post(API_ROUTES.AUTH, payload);

    return response.data;
  }
);
