import { createSlice } from "@reduxjs/toolkit";
import { UserActions } from "@store/actions/user.actions";
import { getUsers } from "@store/thunks/users/users.thunks";
import { UserResponse } from "types/user_schema";

export interface UsersState {
  fetch: UsersFetch;
}

export interface UsersFetch {
  users: UserResponse[];
  loading: boolean;
}

export const initialState: UsersState = {
  fetch: {
    users: [],
    loading: false,
  },
};

export const userSlice = createSlice({
  name: UserActions.NAME,
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getUsers.pending, ({ fetch }: UsersState) => {
        fetch.loading = true;
      })
      .addCase(getUsers.rejected, ({ fetch }: UsersState) => {
        fetch.loading = false;
      })
      .addCase(getUsers.fulfilled, ({ fetch }: UsersState, { payload }) => {
        fetch.loading = false;

        fetch.users = payload;
      });
  },
});

export default userSlice.reducer;
