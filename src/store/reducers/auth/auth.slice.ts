import { LocalStorageKeyEnum } from "@infrastructure/enum";
import { get } from "lodash";
import { createSlice } from "@reduxjs/toolkit";
import { AuthActions } from "@store/actions/auth.actions";
import { signIn } from "@store/thunks/auth/auth.thunks";

export interface AuthState {
  loading: boolean;
}

export const initialState: AuthState = {
  loading: false,
};

export const authSlice = createSlice({
  name: AuthActions.NAME,
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(signIn.pending, (state: AuthState) => {
        state.loading = true;
      })
      .addCase(signIn.rejected, (state: AuthState) => {
        state.loading = false;
      })
      .addCase(signIn.fulfilled, (state: AuthState, { payload }) => {
        state.loading = false;

        localStorage.setItem(
          LocalStorageKeyEnum.AUTH_TOKEN,
          get(payload, "token", "")
        );

        localStorage.setItem(
          LocalStorageKeyEnum.USER,
          JSON.stringify(get(payload, "user", ""))
        );

        window.location.href = "/admin/dashboard";
      });
  },
});

export default authSlice.reducer;
