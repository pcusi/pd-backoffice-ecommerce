import { configureStore } from "@reduxjs/toolkit";

import authSlice from "@store/reducers/auth/auth.slice";
import userSlice from "@store/reducers/user/user.slice";

export const store = configureStore({
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
  reducer: {
    auth: authSlice,
    user: userSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
